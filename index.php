<?php
    include "anuncios.php";
    
    
?>
<!-------------------------------------HTML---------------------------------------------->
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title> examen </title>
        <link rel="stylesheet" href="estilo.css"/>
    </head>
    <body>
        <header>
            <a href="index.php">inicio</a>
            <a href="login.php">autenticacion</a>
            <a href="anuncinou.php">Crea un anuncio</a>
           
        </header>
        <section id="formulario">
            <form name="input" action="<?= $_SERVER['PHP_SELF']?>" method="post">

                <label for="texto">Texto a buscar</label>
                <input type="text" id="texto" required/>
                <br/><br/>

                <label for="minimo">precio mínimo</label>
                <input type="text" id="minimo" placeholder="min"/>
                <br/><br/>

                <label for="maximo">precio máximo</label>
                <input type="text" id="maximo" placeholder="max"/>
                <br/><br/>

                <select name="seleccionar[]">
                    <option value="todos">todos</option>
                    <option value="ultimoDia">ultimo dia</option>
                    <option value="ultimaSemana">ultima semana</option>
                    <option value="ultimoMes">ultimo mes</option>  
                </select>

                <button type="submit" name="enviar">Envia</button>
            </form>
        </section>
        <section id="anuncios">
            <!-- con el objeto creado en el array, llamo a la función mostrarArticulo, que nos devolverá un article con un articulo
            hecho con toda la información de ese objeto-->
           <!--<?php $anuncios[0]->mostrarArticulo();?>
           <br/>
           <br/>
           <?php $anuncios[1]->mostrarArticulo();?> -->
           

           <?php for($i = 0; $i<count($anuncios); $i++){
               $anuncios[$i]->mostrarArticulo();
               echo "<br/> <br/>";
           } ?>


        </section>
    </body>
</html>
