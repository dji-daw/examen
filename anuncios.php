<?php

    //creo un array en el que almacenar los objetos "anuncios"
    $contador = 0;
    $anuncios = [];
    $anuncios[$contador] = new anuncio("Delorean autentico", "25/10/2018", "12:23", "señor jacobo", "autentico delorean en funcionamiento y con todos los papeles en regla. se puede ver sin compromiso", "img/delorean.jpg", "21.500");
    $contador++;
    $anuncios[$contador] = new anuncio("Cervezas baratas", "22/10/2018", "15:21", "El Cervecero", "cervezas a mitad de precio por cese de negocio", "img/imagen1.jpg", "0.50");
    $contador++;

    class anuncio{

        private $titulillo = "probando titulo";
        private $fechilla = "11/10/2018";
        private $horilla = "15:21";
        private $usuario = "probando usuario";
        private $descripcion = "probando descripcion probando descripcionprobando descripcionprobando descripcionprobando descripcionprobando descripcionprobando descripcionprobando descripcionprobando descripcionprobando descripcion";
        private $sourceImagen = "img/imagen1.jpg";
        private $precioArticulo= "55";
        
        
        
        public function __construct($titulillo, $fechilla, $horilla, $usuario, $descripcion, $sourceImagen, $precioArticulo){
            $this->titulillo = $titulillo;
            $this->fechilla = $fechilla;
            $this->horilla = $horilla;
            $this->usuario = $usuario;
            $this->descripcion = $descripcion;
            $this->sourceImagen = $sourceImagen;
            $this->precioArticulo = $precioArticulo;
        }

        //--------GETTERS---------
        public function getTitulillo(){
            return $this->titulillo;
        }
        public function getFechilla(){
            return $this->fechilla;
        }
        public function getHorilla(){
            return $this->horilla;
        }
        public function getUsuario(){
            return $this->usuario;
        }
        public function getDescripcion(){
            return $this->descripcion;
        }
        public function getSourceImagen(){
            return $this->sourceImagen;
        }
        public function getPrecio(){
            return $this->precioArticulo;
        }

        public function mostrarArticulo(){

            echo "<article>";
            echo "<header id='headerArticulo'>";
            echo "<h1>" . $this->titulillo . "</h1>";
            echo "Fecha de creación: " . $this->fechilla . " hora: ". $this->horilla;
            echo "<br/>";
            echo "Creado por: " . $this->usuario;
            echo "</header>";
            echo "<section id='seccionArticulo'>";
            echo "<p>" . $this->descripcion . "</p>";
            echo "<br/>";
            echo "<img src='" . $this->sourceImagen . "'/>";
            echo "</section>";
            echo "<footer id='footerArticulo'>";
            echo "precio: " . $this->precioArticulo . " $.  ";
            //-------------------a la hora de entregar estaba con esto, por lo que está incompleto aunque funcionando.--------------
            echo "<a href='anunci.php?titulo=" . $this->titulillo . "&descripcion=" . $this->descripcion . "&precio=" . $this->precioArticulo . "'>ver anuncio</a>";
            
            echo "</footer>";
            echo "</article>";
        }

    }

?>