<?php
    include "anuncios.php";
    $hora= date ("h:i:s");
    $fecha= date ("j/n/Y");
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        extract($_POST);
       
        $anuncios[$contador] = new anuncio($titulo, $fecha, $hora, "anonimo", $texto, $file, $precio);
        $contador++;
    
    }
?>


<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title> examen </title>
        <link rel="stylesheet" href="estilo.css"/>
    </head>
    <body>
        <header>
            <a href="index.php">inicio</a>
            <a href="login.php">autenticacion</a>
            <a href="anuncinou.php">Crea un anuncio</a>
           
        </header>
        <section id="formulario">
            <form name="input" action="<?= $_SERVER['PHP_SELF']?>" method="post">
                <label for="titulo">Título</label>
                <input type="text" id="titulo" name="titulo" required/>
                <br/><br/>
                <label for="texto">Descripción del artículo</label>
                <input type="text" id="texto" name="texto" required/>
                <br/><br/>
                introduce una imagen
                <input name="file" type="file" size="250kb">
                <br/><br/>
                <label for="precio">Precio: </label>
                <input type="text" name="precio" id="precio"/>
                <button type="submit" name="enviar">Envia</button>
            </form>
        </section>
    </body>
</html>